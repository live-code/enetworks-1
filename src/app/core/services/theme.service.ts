import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  private _theme = 'dark';

  constructor() {
    const currentTheme = localStorage.getItem('theme');
    if (currentTheme) {
      this.theme = currentTheme;
    }
  }

  set theme(newTheme: string) {
    localStorage.setItem('theme', newTheme)
    this._theme = newTheme
  }

  get theme(): string {
    return this._theme;
  }

}
