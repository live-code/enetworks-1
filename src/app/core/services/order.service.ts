import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CartOrder } from '../../model/cart';
import { Video } from '../../model/pexels-video-response';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  order(name: string, surname: string, videos: Video[]) {

    const payload: CartOrder = {
      user: {
        name: name,
        surname: surname
      },
      items: videos.map(v => v.id)
    }

    this.http.post('http://localhost:3000/orders', payload)
      .subscribe(res => {
        this.router.navigateByUrl('/shop')
      })
  }
}
