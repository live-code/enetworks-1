import { Injectable } from '@angular/core';
import { Video } from '../../model/pexels-video-response';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  videos: Video[] = []

  addToCart(video: Video) {
    const isAlreadyOrdered = this.videos.some(v => v.id === video.id)
    if (!isAlreadyOrdered) {
      this.videos.push(video)
    }
  }

  removeFromCart(id: number) {
    const index = this.videos.findIndex(v => v.id === id)
    this.videos.splice(index, 1)
  }

  getTotal() {
    return this.videos.length * 2;
  }
}
