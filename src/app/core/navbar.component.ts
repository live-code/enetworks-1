import { Component, OnInit } from '@angular/core';
import { CartService } from './services/cart.service';
import { ThemeService } from './services/theme.service';

@Component({
  selector: 'app-navbar',
  template: `
    <nav 
      class="navbar navbar-expand "
      [ngClass]="{
        'navbar-light bg-light': themeService.theme === 'light',
        'navbar-dark bg-dark': themeService.theme === 'dark'
      }"
    >
      <a class="navbar-brand" routerLink="home" routerLinkActive="active">
        Enet Demo
        {{themeService.theme}}
      </a>
  
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" routerLink="shop" routerLinkActive="active">shop</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" routerLink="users" routerLinkActive="active">users</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" routerLink="settings" routerLinkActive="active">settings</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" routerLink="contacts" routerLinkActive="active">contacts</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" routerLink="uikit" routerLinkActive="active">uikit</a>
          </li>
          <li class="nav-item">
            <div class="nav-link bg-white text-black rounded-4" routerLink="cart">
              Cart ({{cartService.videos.length}})
            </div>
          </li>
          
        </ul>
      </div>
    </nav>

  `,
  styles: [
  ]
})
export class NavbarComponent {

  constructor(
    public themeService: ThemeService,
    public cartService: CartService
  ) {
    console.log(this.themeService.theme)
  }

}
