import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import {HttpClientModule} from "@angular/common/http";
import { CoreModule } from './core/core.module';
import { HomeComponent } from './features/home/home.component';
import { ShopComponent } from './features/shop/shop.component';
import { CartComponent } from './features/cart/cart.component';
import { UsersExampleComponent } from './features/users-example/users-example.component';
import { Page404Component } from './features/page404/page404.component';
import { NavbarComponent } from './core/navbar.component';
import { SettingsComponent } from './features/settings/settings.component';
import { OrderComponent } from './features/order/order.component';
import { ContactsComponent } from './features/contacts/contacts.component';
import { UikitComponent } from './features/uikit/uikit.component';
import { CardComponent } from './shared/components/card.component';
import { MapquestComponent } from './shared/components/mapquest.component';
import { VideoPlayerModalComponent } from './features/shop/components/video-player-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CartComponent,
    UsersExampleComponent,
    Page404Component,
    OrderComponent,
    ContactsComponent,
  ],
  imports: [
    CoreModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: 'home', component: HomeComponent },

      { path: 'uikit', loadChildren: () => import('./features/uikit/uikit.module').then(m => m.UikitModule) },
      { path: 'shop', loadChildren: () => import('./features/shop/shop.module').then(m => m.ShopModule) },
      { path: 'settings', loadChildren: () => import('./features/settings/settings.module').then(m => m.SettingsModule) },

      { path: 'cart', component: CartComponent },
      { path: 'order', component: OrderComponent },
      { path: 'users', component: UsersExampleComponent },
      { path: 'contacts', component: ContactsComponent },
      { path: '404', component: Page404Component },
      { path: '', component: HomeComponent },
      { path: '**', redirectTo: '404'}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
