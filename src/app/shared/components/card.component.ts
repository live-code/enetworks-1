import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  template: `
    <div class="card">
      <div 
        class="card-header"
        [ngClass]="{
          'bg-dark textwhite ': primary,
          'bg-warning text-white': secondary
        }"
        *ngIf="title"
        (click)="isOpened = !isOpened"
      >
        <div class="d-flex justify-content-between align-items-center">
          {{title}}
          <i
            (click)="iconClick.emit()"
            *ngIf="icon" [class]="icon"
          ></i>
        </div>
      </div>
      <div class="card-body" *ngIf="isOpened">
        <ng-content></ng-content>    
      </div>
    </div>
  `,
  styles: [`
    .textwhite { color: white  }
  `]
})
export class CardComponent {
  @Input() title: string | undefined;
  @Input() primary: boolean = false;
  @Input() secondary: boolean = false;
  @Input() icon: string | undefined;
  @Output() iconClick = new EventEmitter();

  isOpened = true;
}
