import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-mapquest',
  template: `
    <img width="100%"
         [src]="API_URL + '&center=' + city + '&size=200,100&zoom=' + zoom" alt="">
  `,
  styles: [
  ]
})
export class MapquestComponent  {
  @Input() city: string | undefined
  @Input() zoom: number | string = 10;
  API_URL = 'https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn';

}
