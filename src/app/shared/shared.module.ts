import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './components/card.component';
import { MapquestComponent } from './components/mapquest.component';
import { TabBarComponent } from './components/tab-bar.component';



@NgModule({
  declarations: [
    CardComponent,
    MapquestComponent,
    TabBarComponent,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CardComponent,
    MapquestComponent,
    TabBarComponent,
  ]
})
export class SharedModule { }
