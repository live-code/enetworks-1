import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CartService } from '../../core/services/cart.service';
import { PexelVideoResponse, Video } from '../../model/pexels-video-response';

@Component({
  selector: 'app-shop',
  template: `
    
    <app-video-player-modal
      *ngIf="selectedVideo"
      [video]="selectedVideo"
      (close)="selectedVideo = null"
      (addToCart)="cartService.addToCart($event); selectedVideo = null"
    ></app-video-player-modal>
    
    <!-- FORM SEARCH-->
    <div class="d-flex justify-content-center">
      <input 
        type="text" 
        class="form-control form-control-lg"
        placeholder="Search something..."
        style="max-width: 300px"
        #textInput
        (keydown.enter)="searchHandler(textInput.value, orientationInput.value)"
      >
      <select #orientationInput (change)="searchHandler(textInput.value, orientationInput.value)">
        <option value="landscape">Horizontal</option>
        <option value="portrait">Vertical</option>
      </select>
    </div>
    
    <hr>
    
    <!-- RESULT: video list-->
    <div class="d-flex flex-wrap w-100">
      <div 
        *ngFor="let video of videos" 
        class="text-center position-relative m-1"
        (click)="setSelectedVideo(video)"
      >
        <img [src]="video.video_pictures[0].picture" alt="video preview" width="250">
        <div class="position-absolute top-0 end-0 bg-dark text-white rounded-5 px-2 py-1 me-2 mt-2 "> ({{video.duration}} secs)</div>
        <div class="position-absolute bottom-0 end-0 bg-dark text-white rounded-5 px-2 py-1  me-2 mb-2"> 
          <i class="fa fa-cart-plus" 
             (click)="addToCart(video, $event)"></i>  
        </div>
      </div>
    </div>
  `,
})
export class ShopComponent  {
  videos: Video[] = []
  selectedVideo: Video | null = null;

  constructor(private http: HttpClient, public cartService: CartService) {
    this.searchHandler('nature', 'landscape')
  }

  searchHandler(text: string, orientation: string) {
    console.log(text)
    this.http.get<PexelVideoResponse>(`https://api.pexels.com/videos/search?query=${text}&orientation=${orientation}`, {
      headers: {
        Authorization: '563492ad6f9170000100000189ac030285b04e35864a33b95c2838be'
      }
    })
      .subscribe(res => {
        this.videos = res.videos;
      })
  }

  setSelectedVideo(video: Video) {
    console.log('select video', video)
    this.selectedVideo = video;
  }

  addToCart(video: Video, event: MouseEvent) {
    event.stopPropagation();
    this.cartService.addToCart(video)
  }
}
