import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Video } from '../../../model/pexels-video-response';

@Component({
  selector: 'app-video-player-modal',
  template: `
    <div
      class="position-fixed top-0 start-0 end-0 end-0 bg-white w-100 vh-100 p-5 d-flex flex-column justify-content-center align-items-center"
      style="z-index: 10"
    >
      <video
        *ngIf="video"
        width="640" height="480" controls
        [src]="video.video_files[0].link"></video>

      <button
        class="position-absolute top-0 end-0"
        (click)="close.emit()">close</button>

      <i class="fa fa-cart-plus top-0 start-0"
         (click)="addToCart.emit(this.video!)"
         ></i>
    </div>
  `,
  styles: [
  ]
})
export class VideoPlayerModalComponent {
  @Input() video: Video | null = null;
  @Output() close = new EventEmitter();
  @Output() addToCart = new EventEmitter<Video>();
}
