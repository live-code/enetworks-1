import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { VideoPlayerModalComponent } from './components/video-player-modal.component';
import { ShopComponent } from './shop.component';



@NgModule({
  declarations: [
    ShopComponent,
    VideoPlayerModalComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: ShopComponent}
    ])
  ]
})
export class ShopModule { }
