import { Component, OnInit } from '@angular/core';
import { CartService } from '../../core/services/cart.service';

@Component({
  selector: 'app-cart',
  template: `
   
    <div class="alert alert-warning" 
         *ngIf="!cartService.videos.length">
      Non ci sono elementi del carrello
    </div>
    
    <li *ngFor="let item of cartService.videos">
      <img [src]="item.video_pictures[0].picture" width="100">
      <div>
        <button (click)="cartService.removeFromCart(item.id)">-</button>
      </div>
    </li>
    <div>
      TOTAL: {{cartService.getTotal()}} euro
    </div>
    
    <button routerLink="/order" *ngIf="cartService.videos.length">CHECKOUT</button>
  `,
  styles: [
  ]
})
export class CartComponent implements OnInit {

  constructor(public cartService: CartService) { }

  ngOnInit(): void {
  }

}
