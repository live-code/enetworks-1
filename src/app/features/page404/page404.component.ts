import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page404',
  template: `
    <h1>Page does not exists</h1>
    <button routerLink="/home">Go to home</button>
  `,
  styles: [
  ]
})
export class Page404Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
