import { Component, OnInit } from '@angular/core';
import { CardComponent } from '../../shared/components/card.component';

@Component({
  selector: 'app-uikit',
  template: `
    <app-card 
      title="WIDGET"
      icon="fa fa-facebook"
      (iconClick)="openUrl('http://www.facebook.com')"
    > abc</app-card>
    
     <app-card 
       [title]="city"
       [primary]="true"
       icon="fa fa-google"
       (iconClick)="doSomething()"
     >
       <app-mapquest [city]="city" [zoom]="zoom"></app-mapquest>
       <button (click)="zoom = zoom - 1">-</button>
       <button (click)="zoom = zoom + 1">+</button>
       <button (click)="city = 'Trieste'">TS</button>
       <button (click)="city = 'Palermo'">PA</button>
      
     </app-card>
  `,
  styles: [
  ]
})
export class UikitComponent {
  zoom = 10;
  city = 'Milano'

  openUrl(url: string) {
    window.open(url)
  }

  doSomething() {
    console.log('do something')
  }
}
