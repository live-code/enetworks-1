import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CardComponent } from '../../shared/components/card.component';
import { SharedModule } from '../../shared/shared.module';
import { UikitComponent } from './uikit.component';



@NgModule({
  declarations: [
    UikitComponent,
  ],
  imports: [
    SharedModule,
    CommonModule,
    RouterModule.forChild([
      { path: '', component: UikitComponent}
    ])
  ]
})
export class UikitModule { }
