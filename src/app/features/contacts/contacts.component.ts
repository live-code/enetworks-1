import { AfterViewInit, Component, Injectable, OnInit, ViewChild } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';

@Component({
  selector: 'app-contacts',
  template: `
    <form #f="ngForm" (submit)="save(f)">
      <label *ngIf="inputName.errors?.['required']">Required Field</label>
      <label *ngIf="inputName.errors?.['minlength']">
        Missing {{formUtils.missingChars(inputName)}} chars
      </label>
      
      <input 
        type="text"
        class="form-control"
        [ngClass]="{'is-invalid': inputName.invalid && f.dirty}"
        placeholder="Your name"
        name="name"
        [ngModel]="formData.name"
        #inputName="ngModel"
        required minlength="5"
      >
      <div class="my-progress-bar" [style.width.%]="formUtils.getPerc(inputName)"></div>

      <input
        type="text"
        class="form-control"
        [ngClass]="{'is-invalid': inputSurname.invalid && f.dirty}"
        placeholder="Your surname"
        name="surname"
        [ngModel]="formData.surname"
        #inputSurname="ngModel"
        required minlength="3"
      >
      <div class="my-progress-bar" [style.width.%]="formUtils.getPerc(inputSurname)"></div>
      
      <select 
        [ngModel]="formData.gender" name="gender" required class="form-control"
        #inputGender="ngModel"
        [ngClass]="{'is-invalid': inputGender.invalid && f.dirty}"
      >
        <option value="">Select Gender</option>
        <option value="M">Male</option>
        <option value="F">Female</option>
      </select>
      
      <input 
        type="checkbox" name="confirmedPrivacy" [ngModel]="formData.confirmedPrivacy" required
        #checkboxPrivacy="ngModel"
      >
      <span [style.color]="checkboxPrivacy.invalid && f.dirty ? 'red' : null">Privacy</span>
      
      <br>
      <button type="submit" [disabled]="f.invalid" >save</button>
    </form>
    
    <button (click)="populate()">POPULATE</button>
  `,
  styles: [`
    .my-progress-bar {
      width: 100%;
      height: 5px;
      background: red;
      transition: width 0.5s ease-in-out 
    }
  `]
})
export class ContactsComponent  {
  @ViewChild('f') form!: NgForm; //
  formData = { name: '', surname: '', gender: '', confirmedPrivacy: false}

  constructor(public formUtils: FormUtils) {}

  save(form: NgForm) {
    console.log('HTTP POST', form.value)
    form.reset({  gender: ''});
  }

  populate() {
    this.formData = { name: 'a', surname: 'b', gender: 'M', confirmedPrivacy: true}
  }
}

// =============================
// core/services/forms.utils.ts
// =============================
@Injectable({ providedIn: 'root'})
export class FormUtils {
  missingChars(input: NgModel) {
    return input.errors?.['minlength']['requiredLength'] - input.errors?.['minlength']['actualLength']
  }

  getPerc(input: NgModel): number {
    const minLength = input.errors?.['minlength'];
    if (minLength)  {
      return (minLength['actualLength'] / minLength['requiredLength'] ) * 100
    } else {
      return 0
    }
  }
}
