import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/services/theme.service';

@Component({
  selector: 'app-settings',
  template: `
    
    <app-card title="Settings">
      <h3>Current Theme: {{themeService.theme}}</h3>
      <button (click)="themeService.theme = 'dark'">Dark</button>
      <button (click)="themeService.theme = 'light'">Light</button>  
    </app-card>
    
  `,
  styles: [
  ]
})
export class SettingsComponent implements OnInit {

  constructor(public themeService: ThemeService) {

  }

  ngOnInit(): void {
  }

}
