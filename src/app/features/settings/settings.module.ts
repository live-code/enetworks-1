import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CardComponent } from '../../shared/components/card.component';
import { SharedModule } from '../../shared/shared.module';
import { SettingsComponent } from './settings.component';



@NgModule({
  declarations: [
    SettingsComponent,
  ],
  imports: [
    SharedModule,
    CommonModule,
    RouterModule.forChild([
      { path: '', component: SettingsComponent}
    ])
  ]
})
export class SettingsModule { }
