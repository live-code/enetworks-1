import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from '../../core/services/cart.service';
import { OrderService } from '../../core/services/order.service';
import { CartOrder } from '../../model/cart';
import { CartComponent } from '../cart/cart.component';

@Component({
  selector: 'app-order',
  template: `
    SUMMARY ORDER: {{cartService.getTotal()}}
    <hr>
    <h1>Your Info</h1>
    <input type="text" placeholder="name" #inputName>
    <input type="text" placeholder="surname" #inputSurname>
    <hr>
    <button
      *ngIf="cartService.videos.length"
      (click)="proceed(inputName.value, inputSurname.value)">Pay</button>
  `,
})
export class OrderComponent  {
  constructor(
    public cartService: CartService,
    public orderService: OrderService,
  ) { }

  proceed(name: string, surname: string) {
    this.orderService.order(name, surname, this.cartService.videos)
  }
}
