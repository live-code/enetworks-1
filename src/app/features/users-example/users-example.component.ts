import { Component } from '@angular/core';
import {User} from "../../model/user";
import {HttpClient} from "@angular/common/http";
import { UsersService } from './services/users.service';

@Component({
  selector: 'app-users-example',
  template: `
    <div class="container">
      <div class="alert alert-danger" *ngIf="usersService.error">AHia!!!</div>
      
      <i class="fa fa-plus-circle fa-2x" 
         (click)="usersService.setActiveUser({})"></i>
      
      <!--MODAL EDIT / ADD (upsert)-->
      <div
        *ngIf="usersService.activeUser"
        class="position-absolute top-0 bottom-0 start-0 end-0 bg-dark w-100 vh-100" style="z-index: 10"
      >
        <form #f="ngForm" 
              (submit)="usersService.saveUser(f.value)">
          <input type="text" 
                 [ngModel]="usersService.activeUser.name" 
                 name="name" 
                 class="form-control"
                 placeholder="Add Name">
          
          <button type="submit">
            {{usersService.activeUser.id ? 'EDIT' : 'ADD'}}
          </button>
          <button
            (click)="usersService.setActiveUser(null)"
            *ngIf="usersService.activeUser?.id">CLEAR</button>
        </form>
      </div>
      
      
      <!--USER LIST-->
      <h1>User list </h1>
      <ul class="list-group">
        <li
          *ngFor="let user of usersService.users"
          class="list-group-item"
          [ngClass]="{active: user.id === usersService.activeUser?.id}"
          (click)="usersService.setActiveUser(user)"
        >
          {{user.name}}
          <i class="fa fa-trash" (click)="usersService.deleteUser(user.id, $event)"></i>
        </li>
      </ul>
    </div>
  `,
})
export class UsersExampleComponent {
  constructor(public usersService: UsersService) {
    this.usersService.loadUsers();
  }
}
