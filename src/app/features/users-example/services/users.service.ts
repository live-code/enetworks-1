import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../../../model/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  users: User[] = [];
  error = false;
  activeUser: Partial<User> | null = null;

  constructor(private http: HttpClient) {
  }

  loadUsers() {
    this.http.get<User[]>('http://localhost:3000/users')
      .subscribe({
        next: (res) => {
          this.users = res;
        },
        error: () => {
          this.error = true;
        }
      })
  }

  deleteUser(idToRemove: number, event: MouseEvent) {
    event.stopPropagation()
    this.http.delete<void>(`http://localhost:3000/users/${idToRemove}`)
      .subscribe(
        () => {
          const index = this.users.findIndex(u => u.id === idToRemove)
          this.users.splice(index, 1)
        }
      )
  }

  saveUser(formData: Partial<User>) {
    if (this.activeUser?.id) {
      this.editUser(formData)
    } else {
      this.addUser(formData)
    }
  }

  addUser(formData: Partial<User>) {
    this.http.post<User>(`http://localhost:3000/users/`, formData)
      .subscribe(res => {
        this.users.push(res);
        this.setActiveUser(null)
      })
  }

  editUser(formData: Partial<User>) {
    this.http.patch<User>(`http://localhost:3000/users/${this.activeUser?.id}`, formData)
      .subscribe(res => {
        const index = this.users.findIndex(u => u.id === this.activeUser?.id)
        this.users[index] = res;
        this.setActiveUser(null)
      })
  }

  setActiveUser(user: any) {
    this.activeUser = user
  }
}
