
export interface CartOrder {
  user: {
    name: string,
    surname: string
  },
  items: number[]
}
